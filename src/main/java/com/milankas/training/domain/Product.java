package com.milankas.training.domain;

import lombok.*;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private UUID productId;

    @Size(min = 2, max = 30)
    private String productName;

    @Size(min = 2, max = 50)
    private String productDescription;

    private UUID companyId;

    private Boolean blocked;

    private List<String> categories;
}
