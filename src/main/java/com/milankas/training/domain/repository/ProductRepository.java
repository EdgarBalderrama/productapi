package com.milankas.training.domain.repository;

import com.milankas.training.domain.Product;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductRepository {

    List<Product> findAll(Product product);
    Optional<Product> getProduct(UUID productId);
    Product save(Product product);
    void delete(UUID productId);
}
