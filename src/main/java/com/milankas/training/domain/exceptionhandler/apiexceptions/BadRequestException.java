package com.milankas.training.domain.exceptionhandler.apiexceptions;

public class BadRequestException extends RuntimeException{

    public BadRequestException(String message) {
        super(message);
    }
}
