package com.milankas.training.domain.exceptionhandler.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ApiError {

    private String message;
    private HttpStatus status;
    private Date timestamp;
    private List<ApiErrorDetail> errorDetails;

    public ApiError(String message, HttpStatus status, List<ApiErrorDetail> errorDetails) {
        this.message = message;
        this.status = status;
        this.errorDetails = errorDetails;
        this.timestamp = new Date();
    }

    public ApiError(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
        this.timestamp = new Date();
    }
}
