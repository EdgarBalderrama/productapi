package com.milankas.training.domain.service;

import com.milankas.training.domain.Product;
import com.milankas.training.domain.messagebroker.MessageBroker;
import com.milankas.training.domain.repository.ProductRepository;
import com.milankas.training.domain.exceptionhandler.apiexceptions.NotFoundException;
import com.milankas.training.domain.utils.MyBeansUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final MessageBroker messageBroker;

    public ProductService(ProductRepository productRepository, MessageBroker messageBroker) {
        this.productRepository = productRepository;
        this.messageBroker = messageBroker;
    }

    public List<Product> findBy(String name, String description){
        Product product = new Product();
        product.setProductName(name);
        product.setProductDescription(description);

        messageBroker.send("Status Code: 200 ok. HTTP Method: Get. Message: Find products");
        return productRepository.findAll(product);
    }

    public Product getProduct(UUID productId){
        Optional<Product> product = productRepository.getProduct(productId);
        if (!product.isPresent()){
            throw new NotFoundException("Product does not exists");
        }
        return product.get();
    }

    public Product getProductForRabbitMessage(UUID productId){
        Optional<Product> product = productRepository.getProduct(productId);
        if (!product.isPresent()){
            throw new NotFoundException("Product does not exists");
        }
        messageBroker.send("Status Code: 200 ok. HTTP Method: Get. Response: Get product by ID: " + productId);
        return product.get();
    }

    public Product save(Product product){
        messageBroker.send("Status Code: 200 ok. HTTP Method: Post. Response: New product saved");
        return productRepository.save(product);
    }

    public Product update(UUID id, Product userProduct){
        messageBroker.send("Status Code: 200 ok. HTTP Method: Patch. Response: Product updated");

        Product product = getProduct(id);
        MyBeansUtil<Product> cloner = new MyBeansUtil<>();
        product = cloner.copyNonNullProperties(product, userProduct);

        return productRepository.save(product);
    }

    public void delete(UUID productId){
        Product product = getProduct(productId);
        messageBroker.send("Status Code: 200 ok. HTTP Method: Delete. Response: Product deleted");
        productRepository.delete(product.getProductId());
    }
}
