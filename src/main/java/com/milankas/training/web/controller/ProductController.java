package com.milankas.training.web.controller;


import com.milankas.training.domain.Product;
import com.milankas.training.domain.service.ProductService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> getByParams(@RequestParam(value = "name", required = false) String name,
                                     @RequestParam(value = "description", required = false) String description) {
        return productService.findBy(name, description);
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable("id") UUID productId){
        return productService.getProductForRabbitMessage(productId);
    }

    @PostMapping()
    public Product save(@Valid @RequestBody Product product){
        return productService.save(product);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") UUID productId){
        productService.delete(productId);
    }

    @PatchMapping("{id}")
    public Product update(@PathVariable("id") UUID id, @Valid @RequestBody Product userProduct) {
        return productService.update(id, userProduct);
    }
}
