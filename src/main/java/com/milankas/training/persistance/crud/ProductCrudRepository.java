package com.milankas.training.persistance.crud;

import com.milankas.training.persistance.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.UUID;


public interface ProductCrudRepository extends JpaRepository<ProductEntity, UUID> { }
