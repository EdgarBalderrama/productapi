package com.milankas.training.persistance.entity;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "products")
@Data
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class ProductEntity {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    private String description;

    @Column(name = "company_id")
    private UUID companyId;

    private Boolean blocked;

    @Type(type = "list-array")
    @Column(
            name = "categories",
            columnDefinition = "text[]"
    )
    private List<String> categories;
}
