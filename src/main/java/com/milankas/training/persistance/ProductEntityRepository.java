package com.milankas.training.persistance;

import com.milankas.training.domain.Product;
import com.milankas.training.domain.repository.ProductRepository;
import com.milankas.training.persistance.crud.ProductCrudRepository;
import com.milankas.training.persistance.entity.ProductEntity;
import com.milankas.training.persistance.mapper.ProductMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Repository
public class ProductEntityRepository implements ProductRepository {

    private final ProductCrudRepository productCrudRepository;
    private final ProductMapper productMapper;

    public ProductEntityRepository(ProductCrudRepository productCrudRepository, ProductMapper productMapper) {
        this.productCrudRepository = productCrudRepository;
        this.productMapper = productMapper;
    }

    @Override
    public Optional<Product> getProduct(UUID productId) {
        return productCrudRepository.findById(productId).map(productMapper::toProduct);
    }

    @Override
    public List<Product> findAll(Product product) {
        ProductEntity productEntity = productMapper.toProductEntity(product);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withMatcher("description", contains());

        Example<ProductEntity> productEntityExample = Example.of(productEntity, matcher);
        List<ProductEntity> products = productCrudRepository.findAll(productEntityExample);

        return productMapper.toProducts(products);
    }

    @Override
    public Product save(Product product) {
        ProductEntity productEntity = productMapper.toProductEntity(product);
        return productMapper.toProduct(productCrudRepository.save(productEntity));
    }

    @Override
    public void delete(UUID productId) {
        productCrudRepository.deleteById(productId);
    }
}
