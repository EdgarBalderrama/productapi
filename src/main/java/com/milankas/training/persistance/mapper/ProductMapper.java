package com.milankas.training.persistance.mapper;

import com.milankas.training.domain.Product;
import com.milankas.training.persistance.entity.ProductEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mappings({
            @Mapping(source = "id", target = "productId"),
            @Mapping(source = "name", target = "productName"),
            @Mapping(source = "description", target = "productDescription"),
            @Mapping(source = "companyId", target = "companyId"),
            @Mapping(source = "blocked", target = "blocked"),
            @Mapping(source = "categories", target = "categories"),
    })
    Product toProduct(ProductEntity productEntity);
    List<Product> toProducts(List<ProductEntity> productEntities);

    @InheritInverseConfiguration
    ProductEntity toProductEntity(Product product);
}
